# VueLabs

## About
This is a series of steps towards doing the Hands-On-Labs for VueJS training. Each lab step is in its own Git branch, so that each step can be done independently if desired.

## Requirements

This tutorial assumes that you are a develper familiar with Web tools and languages, such as HTML, CSS and ECMAScript (commonly called Javascript). If you've never built a Web application before, this is probably not the right place to start. It's also assumed that you're comfortable with Git, which we use to provide you with starting points for each step in the lab manual.

## Resources

Slides for this workshop can be found [here](http://www.newardassociates.com/slides/Workshops/Vue.pdf).
VueJS Documentation guide can be found [here](https://vuejs.org/v2/guide/).
VueJS API reference is [here](https://vuejs.org/v2/api/), and a cookbook of examples is [here](https://vuejs.org/v2/cookbook/).
If you need a guide to various ECMAScript 6 features, [here you go](http://es6-features.org/#Constants).

## Setup
There are a few things you should have installed on your machine before starting these labs. If they aren't currently installed, now's a good time to get them:

* [NodeJS](https://nodejs.org): Download the "LTS" (Long-Term Support) version for your platform, or use your platform's favorite package manager to install Node. If you have Node already installed, verify you have a relatively recent version by running `node -v`; anything after 6.x should work fine.

* [Git](https://git-scm.com): This lab assumes you have some familiarity with Git at the command-line, but not much is required. If you choose to use Git from a tool other than the command-line (such as from within SourceTree or VisualStudio), you will need to be able to check out branches.

* Text editor: You will need a text editor with which you are somewhat comfortable; I like [VSCode](https://code.visualstudio.com), but this is discussion in which reasonable people can reasonably disagree. The assumption is that during the labs, we will be running commands from the command-line, so you may find it a more seamless/smooth experience if you use an editor with an integrated terminal.

* A recent version of a modern browser: Usually this means Chrome latest, but recent versions of any modern browser should also work. ***NOTE:*** VueJS does not work with any browser prior to IE 8! There is no polyfill support for a particular feature of ECMAScript that VueJS uses at the heart of its reactivity support, so this is a non-negotiable and non-workaroundable restriction.

Before the workshop begins, you will also want to use the Node npm package manager to install the Vue Command-Line Interface (CLI) tool, by `npm install -g @vue/cli` in a command terminal window of your choice. Verify that the installation worked by running `vue --version` and noting the version number it returns. As of this writing, it returns "3.1.3" (which is the version of the CLI tool itself, not VueJS as a whole).

At this point, you are ready to begin the lab with part 1. To see the instructions for part 1, do a "git checkout lab-1" in a command-line terminal.











